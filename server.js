var express= require('express'),
    app= express(),
    ip = require('ip');

app.listen(3000, function() {
  console.log("Server started");
  console.log("My id: "+ip.address());
});

app.get("/", (req, res)=>{
  res.send("Hello world");
});

app.get("/a", (req, res)=>{
  res.send("Hello a");
});
